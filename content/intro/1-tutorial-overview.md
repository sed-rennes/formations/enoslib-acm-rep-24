+++
title = "Tutorial overview"
weight = 1
+++

The tutorial is organized in **a 3 hours workshop** (pause included :)) 
with **many practical sessions**

--- 

**Tutorial Goals:**
- learn about experimental research platform (more precisely: [Grid'5000](https://grid5000.fr))
- acquire resources on such platforms and understand services offers by such
platform (especially in terms of controlling the experimental environment)
- use high level scripting library ([EnOSlib](https://discovery.gitlabpages.inria.fr)) to orchestate the experimentation
- understand why such platform and library can be beneficial to reproducibility and replicability


---

We first introduce the [Grid'5000 platform](@/intro/2-grid5000/index.md) on which we will deploy our
experimental artifact. We then next give a quick overview of [EnOSlib](@/intro/3-enoslib.md)

[In the first part](@/part1/_index.md) of the tutorial, participants will use Jupyter notebooks to
acquire hands-on experience with EnOSlib. They will learn how to use EnOSlib to
run experiments using real hardware provided by the Grid’5000 platform (part of
SLICES). At the end of this part, participants are expected to know how to
compose some of the EnOSlib’s functions to build a simple experimental artifact allowing the
distributed system under study to be deployed and instrumented.

[In the second part](@/part2/_index.md) of the tutorial the participants will be guided to use
EnOSlib to build their own experimental artifact involving parameter sweeping
over the infrastructure settings (e.g disk technology, network topology)

