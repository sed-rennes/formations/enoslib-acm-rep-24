+++
title = "Part 1: Discovering EnOSlib"
weight = 2
sort_by = "weight"
insert_anchor_links = "right"
+++


The goal of this part is to discover Grid'5000 and how it can be used from EnOSlib library.
Due to time constraints, we propose to run the following notebooks:

- `notebooks/g5k/01_remote_actions_and_variables.ipynb` in which you'll start to
play with Grid'5000 resources and some of the basic EnOSlib features
(reservations and remote actions)
- `notebooks/g5k/02_environment_control_resource_selection.ipynb` in which you'll discover how specific resources can be reserved from EnOSlib (nodes, networks and disks)
- `notebooks/g5k/03_observability_service.ipynb` where you'll learn about EnOSlib's service (facilities offered to the experimenter to instrument easily its experimental artifact)

These notebooks will serve as an introduction to the second part, where a **realistic** experimental artifact will be built ! 🎉