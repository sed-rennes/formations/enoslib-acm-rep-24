+++
title = "Part 2: End-to-end experiments with EnOSlib"
weight = 3
sort_by = "weight"
insert_anchor_links = "right"
+++

We will now work with a complete end-to-end experiment.  The goal is to
benchmark [Garage](https://garagehq.deuxfleurs.fr/), an open-source
distributed object storage service tailored for self-hosting.  We will
benchmark Garage with various hardware and network configuration.

To speed up the tutorial, we already provide the experiment-specific
logic.  You will have to extend this experiment in various way to learn
more about EnOSlib.  We encourage you to reuse this experiment structure
for other experiments (e.g. reproduce the results of a distributed system
article)

When you're ready, [jump to the setup](@/part2/1-setup.md).
