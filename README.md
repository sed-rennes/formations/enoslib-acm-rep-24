# EnOSlib tutorial for ACM REP'24

## Access the tutorial

<https://sed-rennes.gitlabpages.inria.fr/formations/enoslib-acm-rep-24>

## Using Zola to build the content

For local development:

- [Install Zola](https://www.getzola.org/documentation/getting-started/installation/).  I tend to use the version from Snap, or the Docker image.
- Run `zola serve`
- If there are no errors, the tutorial is accessible locally on <http://127.0.0.1:1111>
- Zola will automatically rebuild the content when you change something

When you push, Gitlab CI will automatically rebuild the site and upload it to <https://sed-rennes.gitlabpages.inria.fr/formations/enoslib-acm-rep-24>

You might want to run `zola check` from time to time to check the validity of external links.

