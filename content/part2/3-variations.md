+++
title = "Vary the experiment"
weight = 3
+++

You will now try to make variations of the experiment by changing its inputs.

## Designing inputs

Copy the provided input file `input/example.yaml` and use it as a base for
your own inputs.

You should be able to design inputs that answer the following research
questions:

- is Garage performance sensitive to the CPU model?
- is Garage performance sensitive to the disk technology (HDD, SSD)?  For
  its metadata storage or for its data storage?
- does Garage performance depend heavily on the latency between nodes?

Explore the [Grid'5000 Hardware
documentation](https://www.grid5000.fr/w/Hardware) to build your inputs.

To obtain meaningful results, you should vary the parameter of interest
while trying to keep all other parameters the same.  Note that this is
never completely possible: the hardware provided by Grid'5000 is very
diverse, but it obviously cannot provide all possible combinations of
parameters.
