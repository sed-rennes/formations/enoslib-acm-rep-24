+++
title = "Introduction"
weight = 1
sort_by = "weight"
insert_anchor_links = "right"
+++


This is the companion website of the [EnOSlib](https://discovery.gitlabpages.inria.fr/enoslib/index.html)'s tutorial given at [ACM REP 2024](https://acm-rep.github.io/2024/). 


---

👉 **If you don't have a G5k account**, please add you `email, firstname, lastname`
in [this file](https://notes.inria.fr/RbDCDHr_T4qgspjfx-F6bA#) to be registered
to the platform.