+++
title = "About EnOSlib"
weight = 2
+++


**Disclaimer**: EnOSlib is not designed for training an AI model (Ew!) nor to run
computational tasks: in such cases you often reserve the "best" compute (biggest,
fastest ...) resources.

EnOSlib is designed for cases where 
- the **infrastructure is a parameter** of your experimentation and you want to
make this parameter vary. 
- you want to **observe the deployed system** in face of various events (including
infrastructure changes)

---

**The goal:** Help you to easily build and share experimental artifacts by integrating many experimental helpers.

---

**How** : EnOSlib is a Python **library** that exposes various functions
to 
- (a) interface with various platforms; 
- (b) interact programmatically with remote
resources: compute (servers, containers) and networks (IPv4,IPv6); 
- (c) deploy ready-to-use instrumentations services (e.g observability tools, network
emulation);
- (d) ease the post-mortemanalysis phase. At the time of writing, EnOSlib has
been used in several identified scientific publications and in teaching
tutorials

Link to the documentation: [https://discovery.gitlabpages.inria.fr/enoslib/](https://discovery.gitlabpages.inria.fr/enoslib/)