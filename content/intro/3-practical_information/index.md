+++
title = "Practical information"
weight = 3
+++


## Setup

Once your account is validated

1. Connect to [https://intranet.grid5000.fr/notebooks](https://intranet.grid5000.fr/notebooks)
2. Start a new jupyter lab instance **on a frontend** node. Pick a frontend of your choice (but not Toulouse nor Strasbourg[^1])


![start_frontend.png](img/start_frontend.png)

3. Open a terminal and clone in your home dir the tutorial sources: 

```bash
# move back to your home (you don't have write permissions where you land by default)
cd

# clone it  (this is the source of this website)
git clone https://gitlab.inria.fr/sed-rennes/formations/enoslib-acm-rep-24

# get the notebooks (they are stored as a submodule)
cd enoslib-acm-rep-24
git submodule init && git submodule update
```

![clone_frontend.png](img/clone_frontend.png)


4. Open the first notebook: `setup_for_use_in_g5k.ipynb` and proceed with the initial setup.


[1]: https://notes.inria.fr/RbDCDHr_T4qgspjfx-F6bA

---
footnotes:

[^1]: these are new sites, support for notebooks is coming soon