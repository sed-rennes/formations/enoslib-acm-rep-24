+++
title = "Environment setup"
weight = 1
+++

This experiment is provided as a Python script.  While a Jupyter notebook
is nice for interactive development, a script is often easier to automate.
For instance, you can run several successive experimental campaigns with
different inputs.  You can maintain your script in a git repository and
record exactly which version is used for which campaign.

The code is available here: <https://gitlab.inria.fr/bjonglez/garage-g5k-enoslib>

## Connecting to Grid'5000

We recommend running this experiment directly on a Grid'5000 frontend.

If you already have a [Jupyter
notebook](https://intranet.grid5000.fr/notebooks) open on a frontend, you
can work in a terminal from the notebook.

Otherwise, you can connect to a frontend (here in Grenoble) through SSH:

    ssh -J access.grid5000.fr grenoble

If this doesn't work, you may have to [add a SSH key to your Grid'5000
account](https://api.grid5000.fr/stable/users/#myaccount) first.

## Setting up the environment

Clone the repository:

    cd
    git clone https://gitlab.inria.fr/bjonglez/garage-g5k-enoslib
    cd garage-g5k-enoslib

Then create a python virtualenv:

    python3 -m venv venv-garage

Finally, install dependencies (including EnOSlib):

    . venv-garage/bin/activate
    python -m pip install -r requirements.txt
