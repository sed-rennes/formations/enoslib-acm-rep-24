+++
title = "About Grid'5000"
weight = 2
+++


Quoted from the documentation:

> [Grid'5000](https://www.grid5000.fr/w/Grid5000:Home) is a large-scale and flexible testbed for experiment-driven research in all areas of computer science, with a focus on parallel and distributed computing including Cloud, HPC and Big Data and AI.


![G5k-backbone.png](img/G5k-backbone.png)

> Key features:
> - provides access to a large amount of resources: 15000 cores, 800 compute-nodes grouped in homogeneous clusters, and featuring various technologies: PMEM, GPU, SSD, NVMe, 10G and 25G Ethernet, Infiniband, Omni-Path (link to the hardware page: [https://www.grid5000.fr/w/Hardware](https://www.grid5000.fr/w/Hardware))
> - highly reconfigurable and controllable: researchers can experiment with a fully customized software stack thanks to bare-metal deployment features, and can isolate their experiment at the networking layer (link to some of the corresponding tutorials: [deploy your own OS](https://www.grid5000.fr/w/Getting_Started#Deploying_your_nodes_to_get_root_access_and_create_your_own_experimental_environment) / [network isolation](https://www.grid5000.fr/w/KaVLAN))
> - advanced monitoring and measurement features for traces collection of networking and power consumption, providing a deep understanding of experiments
> - designed to support Open Science and reproducible research, with full traceability of infrastructure and software changes on the testbed
> - a vibrant community of 500+ users supported by a solid technical team