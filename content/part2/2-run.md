+++
title = "Run the experiment"
weight = 2
+++

## First run

First, run the experiment as it is provided.  It uses the [virtualenv you created in the setup](@/part2/1-setup.md):

    . venv-garage/bin/activate
    python src/garage_experiment.py input/example.yaml

The script should take less than 10 minutes to complete.  If it doesn't
work, it means that we have failed our first reproducibility test :-)

## Analysis of the results

Run the provided notebook: `garage-analysis.ipynb`

## Reverse-engineering of the experiment design

Take a good look at `input/example.yaml`, and try to understand the design
of this experiment.  What does it try to measure?
