+++
title = "Extend the experiment"
weight = 4
+++

The next step is to **extend** the experiment.  This time, you will modify
the experiment code itself, not just its input.

This last part of the tutorial is meant to be open: you can do it in any
order, and you can add your own ideas!

## Deploy a different operating system

**Research question:** are the results dependent on the operating system
of the nodes?

Deploying a supported operating system is [very easy on
Grid'5000](https://discovery.gitlabpages.inria.fr/enoslib/tutorials/grid5000.html).
This is one of the main use-case of Grid'5000: bare-metal reconfiguration
to the level of the operating system!

**Suggestion:** extend the code and the inputs to test various versions of
Debian and Ubuntu.

### Deploy a different family of operating system (advanced)

Note that the benchmarking code currently assumes that the client node
runs a Debian-like system.  Can you spot where? If you are proficient with
Ansible, you can try to extend it to support CentOS-like systems.

## Change Garage configuration

**Research question:** Garage provides [different replication factors and
consistency
modes](https://garagehq.deuxfleurs.fr/documentation/reference-manual/configuration/#top-level-configuration-options),
do they have an impact on performance?

The provided experiment code takes a straightforward approach to
configuring Garage: while it may not be the most elegant, it should be
easy enough to extend.

**Suggestion:** extend the code and the inputs to change Garage's
replication factor and consistency mode. Test your experiment on a Garage
cluster that spans several Grid'5000 sites, so that you have latency
between your nodes.

## Add emulated network latency between nodes

**Research question:** how much does Garage latency depends on the network
latency between nodes?

Here, we don't just want a binary answer, because it is very likely to be
"yes".  We want to measure the actual impact of network latency on Garage
performance.  As such, we can't just rely on having nodes in different
Grid'5000 sites: that would provide too few data points, and it would vary
too many factors such as CPU model between runs.

A common solution for this kind of research question is **network
emulation**.  It means using real hardware, while adding additional
network constraints using software (e.g. latency, packet loss...).  This
is commonly done with `netem` on Linux or `dummynet` on BSD.

**Suggestion:** Look at [netem support in
EnOSlib](https://discovery.gitlabpages.inria.fr/enoslib/apidoc/netem.html)
and integrate it in the experiment.  Emulated latency should be
configurable from the input to be able to "sweep" over several latency
values and get a good overview.

## Identify a CPU bottleneck

**Research question:** is Garage bottlenecked by the CPU capacity of the hardware?

To answer this research question, there are actually two main parts.

The first part is to add CPU monitoring.  EnOSlib [provides several
monitoring
stack](https://discovery.gitlabpages.inria.fr/enoslib/apidoc/monitoring.html)
that you can add to your experiment.

The second part is much more involved: changing the S3 benchmark.  You can
reuse an existing S3 benchmark such as
[hsbench](https://github.com/markhpc/hsbench) and possibly take
inspiration from [Ceph's Python
wrapper](https://github.com/ceph/cbt/blob/master/benchmark/hsbench.py).
